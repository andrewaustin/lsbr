# lsbr

lsbr is a utility for easily displaying git branches across projects.

![lsbr](http://i.imgur.com/Ppe990K.png)

*Project paths and groups are saved to ~/.lsbr/paths.toml*

### Usage

```
> lsbr -a ~/code/go/github.com/me/myproject

> lsbr
myproject master

> lsbr -a ~/code/go/github.com/me/myproject2 -g two

> lsbr
myproject  master
myproject2 master

> lsbr two
myproject2 master
```
