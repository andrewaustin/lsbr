package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/user"
	pathPkg "path"
	"path/filepath"
	"strconv"
	"strings"
	"text/tabwriter"

	"github.com/BurntSushi/toml"
	"github.com/fatih/color"
)

type Path struct {
	Path  string
	Group string
}

type paths struct {
	Path []Path
}

func main() {
	addFlag := flag.String("a", "", "a path to display the branch of")
	groupFlag := flag.String("g", "", "a group to add the path to")

	flag.Parse()
	initConfig()
	if *addFlag != "" {
		err := handleFlags(addFlag, groupFlag)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		if len(os.Args) > 1 {
			displayBranches(os.Args[1])
		} else {
			displayBranches("")
		}
	}
}

func handleFlags(addFlag, groupFlag *string) error {
	isGitPath, err := isGitWorkingTree(*addFlag)
	if err != nil {
		return err
	}

	if dirExists(*addFlag) && isGitPath {
		aPath := Path{Path: *addFlag, Group: *groupFlag}
		newPath := paths{Path: []Path{aPath}}
		f, err := os.OpenFile(getConfigFilePath(), os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			return err
		}
		b := new(bytes.Buffer)
		if err = toml.NewEncoder(b).Encode(newPath); err != nil {
			return err
		}
		_, err = f.Write(b.Bytes())
		if err != nil {
			return err
		}
		err = f.Close()
		if err != nil {
			return err
		}
	} else {
		return errors.New("path is not a valid directory")
	}

	return nil
}

func displayBranches(group string) {
	var allPaths paths

	if _, err := toml.DecodeFile(getConfigFilePath(), &allPaths); err != nil {
		log.Fatal(err)
	}

	colorMap := make(map[string]*color.Color)
	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 4, 4, 1, ' ', 0)
	for _, p := range allPaths.Path {
		branch := getBranch(p.Path)
		if branch != "" {
			_, ok := colorMap[branch]
			if !ok {
				colorMap[branch] = generateNewColor(len(colorMap))
			}

			shouldOutput := false
			if group == "" {
				shouldOutput = true
			} else if group == p.Group {
				shouldOutput = true
			}

			if shouldOutput {
				fmt.Fprintf(w, "%s\t%s\n", pathPkg.Base(p.Path), colorMap[branch].SprintFunc()(branch))
			}
		}
	}
	err := w.Flush()
	if err != nil {
		log.Fatal(err)
	}
}

func getHomePath() string {
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	return usr.HomeDir
}

func initConfig() {
	lsbrDir := getHomePath() + "/.lsbr/"
	if !dirExists(lsbrDir) {
		err := os.Mkdir(lsbrDir, 0700)
		if err != nil {
			log.Fatal(err)
		}
	}

	if !dirExists(getConfigFilePath()) {
		f, err := os.Create(getConfigFilePath())
		if err != nil {
			log.Fatal(err)
		}
		err = f.Close()
		if err != nil {
			log.Fatal(err)
		}
	}
}

func getConfigFilePath() string {
	return getHomePath() + "/.lsbr/paths.toml"
}

func getBranch(path string) string {
	cleanPath := filepath.Clean(path)
	isGitPath, err := isGitWorkingTree(cleanPath)
	if err != nil {
		return ""
	}

	if dirExists(cleanPath) && isGitPath {
		var cmdOutput bytes.Buffer
		brCmd := exec.Command("git", "status", "-b", "-s", "--porcelain")
		brCmd.Dir = cleanPath
		brCmd.Stdout = &cmdOutput
		err := brCmd.Run()
		if err != nil {
			return ""
		}
		output := cmdOutput.String()
		output = strings.Split(output, "\n")[0]
		output = strings.Split(output, "...")[0]
		output = strings.TrimPrefix(output, "## ")
		output = strings.Trim(output, "\n")
		return output
	}

	return ""
}

func dirExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	return false
}

func isGitWorkingTree(path string) (bool, error) {
	var cmdOutput bytes.Buffer
	cmd := exec.Command("git", "rev-parse", "--is-inside-work-tree")
	cmd.Dir = path
	cmd.Stdout = &cmdOutput
	err := cmd.Run()
	if err != nil {
		return false, err
	}
	boolString := cmdOutput.String()
	boolString = strings.Trim(boolString, "\n")
	res, err := strconv.ParseBool(boolString)
	if err == nil {
		return res, nil
	}
	return false, nil
}

func generateNewColor(size int) *color.Color {
	// TODO: Probably want to generate colors with fb/bg in the future
	colorList := []*color.Color{
		color.New(color.FgGreen),
		color.New(color.FgRed),
		color.New(color.FgYellow),
		color.New(color.FgBlue),
		color.New(color.FgMagenta),
		color.New(color.FgCyan),
	}

	colorLen := len(colorList)
	return colorList[(size)%colorLen]
}
