package main

import (
	"github.com/fatih/color"
	"testing"
)

func TestGenerateNewColor(t *testing.T) {
	red := color.New(color.FgRed)
	blue := color.New(color.FgBlue)
	cyan := color.New(color.FgCyan)

	// Colors are just arrays of params
	aColor := generateNewColor(1)
	if !aColor.Equals(red) {
		t.Error("color 1 should be red")
	}

	aColor = generateNewColor(3)
	if !aColor.Equals(blue) {
		t.Error("color 3 should be blue")
	}

	aColor = generateNewColor(5)
	if !aColor.Equals(cyan) {
		t.Error("color 5 should be cyan")
	}

	aColor = generateNewColor(27)
	if !aColor.Equals(blue) {
		t.Error("color 27 should be blue")
	}

	aColor = generateNewColor(5005)
	if !aColor.Equals(red) {
		t.Error("color 5005 should be red")
	}
}
